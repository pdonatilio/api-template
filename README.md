# Node Web-API Template Project

This project is useful to start a web-api project with nodejs + express + CouchDB with a code-first approach
We add some useful technologies like: docker, swagger, JWT, and others will be included as we need

The idea with this _"Skeleton Project"_ is to be quick to start new APIs development.

With this project build running correctly, you can be focused in create our models and new endpoints.

Enjoy.

### The project folder structure

``` 
.
├── assets
│   └── images
│   └── templates
├── configs
├── controllers
│   └── apis
├── docker-compose.yml
├── Dockerfile
├── env-example
├── Makefile
├── middlewares
├── models
├── package.json
├── README.md
├── routes
├── server.js
└── services
```

### To run the project

You only need [git](https://git-scm.com/), [docker](https://www.docker.com/), and [docker-compose](https://docs.docker.com/compose/) installed on you development environment, but you can build using [nodejs](https://nodejs.org/en/) locally if you want.

To manage automatically the containers be ready to use the [makefile](https://opensource.com/article/18/8/what-how-makefile)

We recommend [visual studio code](https://code.visualstudio.com/), to debug inside the container, but, again, you can use your preferred IDE

Use the **env-example** file to prepare the variable environment


### Tips & Trics

**Make commands**

* `make build` - To build your environment - *you will be able to develop with docker*
* `make env` - To compose up containers in detach mode
* `make env-verbose` - To compose up containers in verbose mode
* `make stop` - To stop containers 
* `make restart` - To restart containers 
* `make clean` - To remove the containers and images
* `make object name=objectName tag=tagName` - To create a strucute with models services and controller to iniciate a new database document with endpoint to manipulate it
* `make delete-object name=objectName` - To remove a strucute with models services and controller _**>>>(this action can be undone and don't remove a database document)**_  
* `make swagger` - To update swagger documentation

**Preparing Visual Studio Code to Debug Inside the Container**

1 - Install the [microsoft remote containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) extension

2 - Create the configuration debug file in `.vscode/launch.json` and include the code below:
    
```
{
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Node.Js Attach",
            "port": 9229,
            "request": "attach",
            "skipFiles": [
                "<node_internals>/**"
            ],
            "type": "pwa-node"
        }
    ]
}
```

3 - Build the container's structure using `make build` command

4 - Click on the Remote Containers extension link and select the container api-template

![readme-1](assets/images/readme-01.png)

5 - Click to open the folder and select the `home/node/app` path

![readme-2](assets/images/readme-02.png)

6 - Click to Run Debug (or `Ctrl + Shift + d`) and select the `Node.Js Attach`

![readme-3](assets/images/readme-03.png)

7 - If the debug console output shows no error then you can debug your NodeJs API

So, that's it.