templateModel = require('../models/template')

const model = new templateModel()

const templateService = {

    /**
     * Return a instance from object - not database data
     */
    Get: () => {
        return model.Get()
    },

    /**
     * Expected Params: object fields
     */
    Set: (params) => {
        model.Set(params)
    },

    /**
     * Return an array from database with this data structure
     * 
     * Expected Params
     * 
     *      params = {
     *          filters: {
     *               key: value
     *          } - *optional
     *          fields: [ "fields", "to", "show" ] -*optional
     *          limit: Value -*optional
     *          sort: [{"field": "direction"}], -*direction = [asc,desc] *optional
     *      }
     */
    GetDB: async (params) => {
        return await model.GetDB(params)
    },

    /**
     * Expected Params
     * 
     *      id = string  *optional
     *      params = {
     *          name: string
     *          description: string *optional
     *      }
     */
    SetDB: async (id, params) => {
        return await model.SetDB(id, params)
    },

    /**
     * Expected Params
     * 
     *      id = string
     */

    DelDB: async (id, physical) => {
        return await model.DelDB(id, physical)
    },

}

module.exports = templateService