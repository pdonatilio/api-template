const config = require("../configs/database")
const defaultLimit = require("../configs/config").database.returnLimit
const databaseName = require("../configs/config").database.name
const logger = require("../configs/logger")
const dayjs = require("dayjs")

/**
 * Create a CouchDB instance to collection passed by parameter
 * By default if collection not exists will be created one
 * You can force not create passing false as second parameter
 * 
 *      Parameters
 *          collection: name from CouchDB collection
 *          force: boolean to force create collection and update document
 * 
 *      Object
 *          collection: string
 *          connection: nano.ServerScope
 *          database: nano.DocumentScope
 *          force: boolean
 */
class CouchDB {
    constructor(collection, force = true) {
        this.collection = collection
        this.connection = undefined
        this.database = undefined
        this.force = force
    }

    async connect(collection) {
        try {
            collection = collection == null ? this.collection : collection
            this.connection = await config.OpenConnection(collection)
            try {
                await this.connection.server.db.get(collection.toLowerCase())
            } catch (error) {
                if (!this.force) {
                    logger.error(`Error on selecting couchdb collection ${collection}`, error)
                    return
                }

                await this.connection.server.db.create(collection.toLowerCase())
            }

            this.database = await this.connection.server.db.use(collection.toLowerCase())
        } catch (error) {
            logger.error(`Error connecting to couchdb collection ${collection}`, error)
            return null
        }
    }

    async Insert(document) {
        if (!this.validDocument(document)) {
            return
        }

        try {
            await this.connect()
            document.status = true
            document.created = new Date()
            let response = await this.database.insert(document)
            if (response.ok) {
                response.message = `Successfully insert ${this.messageWithID(response.id, document)}`
                logger.info(response.message)
                this.databaseLog(document.created, response.message, document)
                return response
            }
        } catch (error) {
            logger.error(`Error to insert ${this.messageWithID(null, document)}`, error)
            return
        }
    }

    async Update(id, document) {
        if (!this.validDocument(document)) {
            return
        }

        let items = await this.getById(id)
        if (items == null && this.force) {
            this.Insert(document)
            return

        } else {
            let item = Object.assign({}, items.docs[0])
            try {
                await this.connect()
                Object.entries(document).map(([key, value]) => {
                    item[key] = value
                })
                item.updated = new Date()
                let response = await this.database.insert(item)
                if (response.ok) {
                    response.message = `Successfully update ${this.messageWithID(response.id, document)}`
                    logger.info(response.message)
                    this.databaseLog(item.updated, response.message, item, items.docs[0])
                    return response
                }
            } catch (error) {
                logger.error(`Error to update ${this.messageWithID(null, document)}`, error)
                return
            }
        }
    }

    async Delete(id, physical = false) {
        let items = await this.getById(id)
        if (items == null) {
            return
        }

        let item = Object.assign({}, items.docs[0])
        try {
            await this.connect()
            if (physical && physical != "false") {
                let response = await this.database.destroy(item._id, item._rev)
                if (response.ok) {
                    response.message = `Successfully physical delete to ${this.messageWithID(id, null)}`
                    logger.info(response.message)
                    this.databaseLog(new Date(), response.message, item)
                    return response
                }
            }

            item.status = false
            let response = await this.database.insert(item)
            if (response.ok) {
                response.message = `Successfully delete to ${this.messageWithID(id, null)}`
                logger.info(response.message)
                this.databaseLog(item.updated, response.message, item)
                return response
            }
        } catch (error) {
            logger.error(`Error to delete ${this.messageWithID(id, null)}`, error)
            return
        }
    }

    /**
     * Return an array of documents from collection
     * 
     *      params = {
     *          filters: {
     *               key: value
     *          } - *optional
     *          fields: [ "fields", "to", "show" ] -*optional
     *          limit: Value -*optional
     *          sort: [{"field": "direction"}], -*direction = [asc,desc] *optional
     *      }
     *      
     *      response = {
     *          ok: boolean
     *          collection: [] array of documents
     *          page: number of page from collection response | *optional
     *          limit: limit max used to return data
     *          message: message text with the return 
     *      }
     */
    async List(params) {
        try {

            const query = this.buildQuery(params)
            await this.connect()
            const response = await this.database.find(query)
            return this.prepareResponse(true, response, params)

        } catch (error) {
            const list = {
                collection: undefined,
                page: undefined,
            }
            return this.prepareResponse(false, list, params, error)
        }

    }

    /**
     * Preparing parameters to mango query format
     * 
     *      params = {
     *          filters: {
     *               key: value
     *          }
     *          fields: [ "fields", "to", "show" ] -*optional
     *          limit: Value -*optional
     *          sort: [{"field": "direction"}], -*direction = [asc,desc] *optional
     *      }
     */
    buildQuery(params) {
        let query = {
            selector: {},
            limit: params && params.limit != null ? parseInt(params.limit) : defaultLimit
        }

        if (params && params.field != null) query.field = params.field
        if (params && params.order != null) query.order = params.order

        if (params && params.filters != null) {
            Object.entries(params.filters).forEach(([key, value]) => {
                query.selector[key] = { "$eq": value }
            })

        } else {
            query.selector = {
                "_id": { "$gt": null }
            }
        }

        return query
    }

    validDocument(document) {
        if (document == null && typeof document != "object") {
            logger.warning(`Invalid: ${document}`)
            return false
        }

        return true
    }

    messageWithID(id, document) {
        return `document ${id + " " || ""}into the collection - \n\tcollection -> ${this.collection} \n\tdocument -> ${JSON.stringify(document)}`
    }

    async getById(id) {
        if (id == null) {
            return
        }
        await this.connect()
        const query = this.buildQuery({ filters: { _id: id } })
        let item = await this.database.find(query)

        if (item == null || item.docs[0] == null) {
            return
        }
        return item
    }

    /**
    * Return a prepared response from collection
    * 
    *      status = boolean
    *      list = {
    *          collection: [] array
    *          page: number
    *      }
    *      params = generic object
    *      
    *      response = {
    *          ok: boolean
    *          collection: [] array of documents
    *          page: number of page from collection response | *optional
    *          limit: limit max used to return data
    *          message: message text with the return 
    *      }
    */
    prepareResponse(status, list, params, err) {
        let response = {
            ok: status,
            collection: list.docs,
            page: list.bookmark,
            limit: params && params.limit ? params.limit : defaultLimit,
            message: `${status ? `Sucessfully` : `Error`} on query execution in collection ${this.collection} with params ${JSON.stringify(params)}`
        }

        if (!status) {
            logger.error(response.message, err)
        }
        return response
    }

    async databaseLog(data, message, document, oldVersion) {
        let collection = this.collection
        await this.connect("database-log")
        this.database.insert({
            date: data,
            collection: collection,
            message: message,
            document: document,
            oldVersion, oldVersion,
        })
    }

}

module.exports = CouchDB