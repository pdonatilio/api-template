const model = require('./modelGenerator')
const CouchDB = require('../services/database.service')
const { connection } = require('../configs/database')
const config = require('../configs/config').database

/**
 * Class Template
 * Template to create an couchDB database
 * 
 *      Template:
 *          name: string
 *          description: string
 *      
 *      Internal
 *          database: functions to database manipulations
 * 
 * 
 */
class Template {
    constructor() {
        this.object = {
          name:  undefined,
          descrition: undefined,
        }

        this.database = model.DatabaseFunctions(
            "template",
            ["name"],
            this.object,
        )
    }

    Get(){
       return this.object
    }

    Set(values){
        return model.FillObject(this.object, values)
    }

    //Database Functions
    async GetDB(params){
        return await this.database.List(params, this.database.name)
    }

    async SetDB(id, params){
        return await this.database.Set(id, params, this.database.name, this.database.required)
    }

    async DelDB(id, physical){
        return await this.database.Delete(id, physical, this.database.name,)
    }
}

module.exports = Template