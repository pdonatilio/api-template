CouchDB = require('../services/database.service')

function DatabaseFunctions(name = "", required = [], object = {}) {
    database = {
        name: name,
        required: required,
        List: async (params, dbName) => { 
            const connection = new CouchDB(dbName)
            response = await connection.List(params)
            return response 
        },
        Set: async (id, data, dbName, requiredFields) => {
            if (id == null) {
                isValid = ValidateFields(requiredFields, data)
                if (!isValid.ok) {
                    return isValid
                }    
            }
            const connection = new CouchDB(dbName)
            return await connection.Update(id, data)
        },
        Delete: async (id, physical, dbName) => {
            const connection = new CouchDB(dbName)
            return await connection.Delete(id, physical)
        }
    }

    return database
}

/**
 * return objectToFill updated with the values of objectWithValues
 * 
 */
function FillObject(objectToFill, objectWithValues, removeBlankKeys = true) {
    Object.entries(objectWithValues).map(([key, value]) => {
        if (key in objectToFill) {
            objectToFill[key] = value
        }
    })
    return removeBlankKeys ? removeObjectBlankKeys(objectToFill) : objectToFill
}

/**
 * return if fields are correct to insert into database
 *
 *      requiredFields: array[string]
 *      document: CouchDB Json Format
 */
function ValidateFields(requiredFields, document) {
    let response = { ok: true }
    requiredFields.map(item => {
        documentFields = getObjectKeys(document)
        if (documentFields.indexOf(item) == -1) {
            response = { ok: false, field: item, message: `Required field ${item} not found` }
            return 
        }
    })

    return response
}

/**
 * Return the object key names an array
 * 
 *      document: Object
 *      response: empty Array of strings
 */
function getObjectKeys(document, response = []) {
    Object.entries(document).map(([key, value]) => {
        if (typeof value === "object" && !Array.isArray(value)) {
            getObjectKeys(value, response)
        }
        response = [...response, key]
    })
    return response
}

function removeObjectBlankKeys(object) {
    Object.entries(object).map(([key, value]) => {
        if (typeof value === "object" && !Array.isArray(value)) {
            removeObjectBlankKeys(value)
        }

        if (value === undefined) {
            delete object[key];
        }
    })
    return object
}

module.exports = modelGenerator = {
    FillObject: FillObject,
    ValidateFields: ValidateFields,
    DatabaseFunctions: DatabaseFunctions,
}