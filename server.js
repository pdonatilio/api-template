const server = require('./configs/app')

async function start() {
  await server.start()
}

start()