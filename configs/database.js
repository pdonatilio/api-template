const couchDB = require('nano')
const Agent = require('agentkeepalive')
const util = require('util')
const logger =  require("./logger")
const config = require("./config").database

const database = {
    connection: {},

    OpenConnection: async (database) => {
        try {
            const http = new Agent({
                maxSockets: config.sockets,
                maxKeepAliveRequests: config.aliveRequests,
                maxKeepAliveTime: config.aliveTime,
            })

            this.connection = couchDB(
                {
                    url: `http://${config.user}:${config.password}@${config.name}:${config.port}`,
                    requestDefaults: { "agent": http }
                }
            )
            logger.info(`Successfully connected to database ${config.name} at http://${config.name}:${config.port}`)
            var test = await this.connection.db.use(database)
            return test

        } catch (error) {
            logger.error(`Error connecting to database ${config.name}`, error)
            return undefined
        }
    }
}

module.exports = database