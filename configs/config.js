const dotenv = require('dotenv')

dotenv.config()
const config = {
    environment: process.env.NODE_ENV,
    host: process.env.HOST,
    port: process.env.API_INTERNAL_PORT,
    api : {
        name: process.env.PROJECT_NAME,
        description: process.env.PROJECT_DESCRIPTION,
        version: process.env.npm_package_version,
    },
    database: {
        name: process.env.DB_CONTAINER_NAME,
        host:process.env.HOST,
        port:process.env.DB_INTERNAL_PORT,
        user:process.env.DB_USER,
        password:process.env.DB_PASSWORD,
        sockets: process.env.DB_MAXSOCKETS,
        aliveRequests: process.env.DB_KEEPALIVE,
        aliveTime: process.env.DB_KEEPALIVETIME,
        returnLimit: parseInt(process.env.DB_RETURNLIMIT) || 25
    },
    user: {
        id: null,
        name: null,
    }

}

module.exports = config