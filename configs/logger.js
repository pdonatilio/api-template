const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf, prettyPrint  } = format;

const config = require("./config")

const logFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} - [${label}][${level}]: ${message}`;
});

const logger = createLogger({
    format: combine(
        label({ label: config.api.name }),
        timestamp(),
        logFormat
      ),
    transports : [
        new transports.Console(),
        new transports.File({
            filename: './logs/error.log',
            level: 'error'
        })
    ]
});

module.exports = logger