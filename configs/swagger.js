const swaggerAutogen = require('swagger-autogen')()
const config = require('./config')

const outputFile = './configs/swagger.json'
const endpointsFiles = [
    './controllers/apis/system.js',
    './controllers/apis/v1/template.js',
] //End of Endpoints Files - DON'T REMOVE THIS COMMENTARY

const swaggerConfig = {
    info: {
        version: config.api.version,
        title: config.api.name,
        description: config.api.description,
    },
    host: `${config.host}:${config.port}`,
    basePath: "/api",
    schemes: ['http',],
    consumes: ['application/json'],
    produces: ['application/json'],
    tags: [
        {
            "name": "System",
            "description": "System Administrative Endpoints"
        }
    ],
    securityDefinitions: {
        Bearer: {
            type: 'apiKey',
            name: 'Authorization',
            in: 'header'
        },
    },
    definitions: {
        //Generic definitions
        Response: {
            ok: true,
            collection: [{}],
            page: 1,
            limit: 25,
            message: "String",
        },
        
        Delete: {
            physical: false
        },

        //Model Definitions
        template:{
            name:  "string",
            descrition: "string",
        },
    }
}

swaggerAutogen(outputFile, endpointsFiles, swaggerConfig)