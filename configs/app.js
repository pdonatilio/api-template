const express = require('express');
const bodyParser = require('body-parser')

const config = require("./config")
const logger = require("./logger")
const routes = require('../routes')

const app = express();

start = () => {
    app.listen(config.port, () => {
        app.set('port', config.environment)
        app.set('hostname', config.host)
        app.use(bodyParser.json())
        app.use(bodyParser.urlencoded({
          extended: false
        }))

        routes.init(app)
        logger.info(`Server **${config.api.name}** is listening http on - http://${config.host}:${config.port}`)
    });
}

module.exports = {
    start: start
}