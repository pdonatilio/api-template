# This Makefile offers shortcuts to build and run a node.Js environtment into docker container

#Load .env files
ifneq (,$(wildcard ./.env))
    include .env
    export
endif

#CONTAINERS
build: 
	@echo "=== Running docker files to create environment ==="
	$(MAKE) clean
	docker build -t ${API_IMAGE_DOCKER} .
	docker run -d --name ${API_CONTAINER_NAME} \
		-p ${API_EXTERNAL_PORT}:${API_INTERNAL_PORT} \
		${API_IMAGE_DOCKER}
	docker cp ${API_CONTAINER_NAME}:${API_DOCKER_FOLDER}/node_modules/. node_modules/
	docker stop ${API_CONTAINER_NAME}
	docker rm ${API_CONTAINER_NAME}

env:
	@echo "=== Start Env ==="
	docker-compose up -d
	@echo "=== The docker environment was created ==="
	@echo "=== Env Up ==="

env-verbose:
	@echo "=== Start Env ==="
	docker-compose up
	@echo "=== The docker environment was created ==="
	@echo "=== Env Up ==="

stop:
	@echo "=== Stop Env ==="
	-docker stop ${API_CONTAINER_NAME}
	-docker stop ${DB_CONTAINER_NAME}
	@echo "=== The docker environment was created ==="
	
restart:
	@echo "=== Restart Servers ==="
	-docker stop ${API_CONTAINER_NAME}
	-docker stop ${DB_CONTAINER_NAME}
	docker start ${API_CONTAINER_NAME}
	docker start ${DB_CONTAINER_NAME}
	@echo "=== Servers was restarted ==="

clean:
	-docker stop ${API_CONTAINER_NAME}
	-docker stop ${DB_CONTAINER_NAME}
	-docker rm ${API_CONTAINER_NAME}
	-docker rm ${DB_CONTAINER_NAME}
	-docker rmi ${API_IMAGE_DOCKER}

#PROJECT
object:
	@echo "=== Creating object structure to ${name} with tag ${tag} ==="
	$(MAKE) controller name=${name} tag=${tag}
	$(MAKE) model name=${name} tag=${tag}
	$(MAKE) service name=${name} tag=${tag}
	$(MAKE) route name=${name}
	$(MAKE) swagger-include name=${name}

controller:
	@echo "=== Creating controller ${name} into tag ${tag} ==="
	cp ${TEMPLATE_PATH}/controller ${API_PATH_CONTROLLERS}${name}.js
	sed -i 's/<<TEMPLATE>>/${name}/g' ${API_PATH_CONTROLLERS}${name}.js
	sed -i 's/<<TAG>>/${tag}/g' ${API_PATH_CONTROLLERS}${name}.js

model:
	@echo "=== Creating model ${name} ==="
	cp ${TEMPLATE_PATH}/model ${API_PATH_MODELS}${name}.js
	sed -i 's/<<TEMPLATE>>/${name}/g' ${API_PATH_MODELS}${name}.js
	sed -i 's/<<TAG>>/${tag}/g' ${API_PATH_MODELS}${name}.js

service:
	@echo "=== Creating service ${name} ==="
	cp ${TEMPLATE_PATH}/service ${API_PATH_SERVICES}${name}.service.js
	sed -i 's/<<TEMPLATE>>/${name}/g' ${API_PATH_SERVICES}${name}.service.js
	sed -i 's/<<TAG>>/${tag}/g' ${API_PATH_SERVICES}${name}.service.js

route:
	@echo "=== Creating route to ${name} controller ==="
	sed -i "\/\/End of api Routes/i router.use('/v1', require('../controllers/apis/v1/${name}'))" routes/index.js

swagger-include:
	@echo "=== Include controller into swagger documentation ==="
	sed -i "\/\/End of Endpoints Files/i \ \ \ \ './controllers/apis/v1/${name}.js'," configs/swagger.js
	docker exec -it ${API_CONTAINER_NAME} sh -c "npm run swagger"

delete-object:
	@echo "=== Removing object structure ==="
	-rm ${API_PATH_CONTROLLERS}${name}.js
	-rm ${API_PATH_MODELS}${name}.js
	-rm ${API_PATH_SERVICES}${name}.service.js
	sed -i "/controllers\/apis\/v1\/${name}'/d" routes/index.js
	sed -i "/controllers\/apis\/v1\/${name}.js'/d" configs/swagger.js
	$(MAKE) swagger

swagger:
	@echo "=== Update swagger documentation ==="
	docker exec -it ${API_CONTAINER_NAME} sh -c "npm run swagger"