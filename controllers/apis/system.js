const express = require('express')
const logger = require('../../configs/logger')

let router = express.Router()

router.get('/health', async (req, res, next) => {
    // #swagger.path = '/system/health'
    // #swagger.tags = ['System']
    // #swagger.description = 'Get system healthy status'
    // #swagger.responses[200] = { description: 'The system is healthy.' }
    // #swagger.responses[404] = { description: 'The system is not working.' }

    return res.status(200).json({message: "The system is healty"})
})

module.exports = router