const express = require('express')
const logger = require('../../../configs/logger')
const template = require("../../../services/template.service")

let router = express.Router()

router.get('/template/:id?', async (req, res, next) => {
    // #swagger.path = '/v1/template'
    // #swagger.tags = ['System']
    // #swagger.description = 'Get from template database - for example purpose only'
    // #swagger.parameters['id'] = { in: 'path', description: 'Template ID' }
    // #swagger.responses[200] = { description: 'Return the template parameter list' , schema: { $ref: "#/definitions/Response" } }
    // #swagger.responses[404] = { description: 'The system is not working.' }
    
    params = {}
    if(req.params.id != null) {
        params.filters = {
            _id: req.params.id
        }
    }

    response = await template.GetDB(params)
    return res.status(200).json(response)
})

router.post('/template', async (req, res, next) => {
    // #swagger.path = '/v1/template'
    // #swagger.tags = ['System']
    // #swagger.description = 'Post to template database - for example purpose only'
    /* #swagger.parameters['newTemplate'] = {
        in: 'body',
        description: 'Insert a Template document into database.',
        type: 'object',
        schema: { $ref: "#/definitions/template" }
    } */
    // #swagger.responses[201] = { description: 'Return the template parameter list' }
    // #swagger.responses[404] = { description: 'The system is not working.' }
    const newTemplate = req.body
    response = await template.SetDB(null, newTemplate)
    return res.status(201).json(response)
})

router.put('/template/:id', async (req, res, next) => {
    // #swagger.path = '/v1/template/{id}'
    // #swagger.tags = ['System']
    // #swagger.description = 'Put to template database - for example purpose only'
    // #swagger.parameters['id'] = { description: 'Template ID' }
    /* #swagger.parameters['newTemplate'] = {
        in: 'body',
        description: 'Update a Template document into database.',
        type: 'object',
        schema: { $ref: "#/definitions/template" }
    } */
    // #swagger.responses[204] = { description: 'Return the template parameter list' }
    // #swagger.responses[404] = { description: 'The system is not working.' }
    const id = req.params.id
    const newTemplate = req.body
    response = await template.SetDB(id, newTemplate)
    return res.status(204).json(response)
})

router.delete('/template/del/:id', async (req, res, next) => {
    // #swagger.path = '/v1/template/del/{id}'
    // #swagger.tags = ['System']
    // #swagger.description = 'Delete to template database - for example purpose only'
    // #swagger.parameters['id'] = { description: 'Template ID' }
    /* #swagger.parameters['parameters'] = {
        in: 'body',
        description: 'Parameters to remove document',
        type: 'object',
        schema: { $ref: "#/definitions/Delete" }
    } */
    // #swagger.responses[204] = { description: 'Return the template parameter list' }
    // #swagger.responses[404] = { description: 'The system is not working.' }
    const id = req.params.id
    const physical = req.body.physical 
    response = await template.DelDB(id, physical)

    return res.status(204).json(response)
})

module.exports = router