const express = require('express')
const cors = require('cors')
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('../configs/swagger.json')
const logger = require("../configs/logger")


let router = express.Router()
router.use(cors())
router.use('/system', require('../controllers/apis/system'))
router.use('/v1', require('../controllers/apis/v1/template'))
//End of api Routes - DON'T REMOVE THIS COMMENTARY

const init = (server) => {
    server.get('*', function (req, res, next) {
        logger.info(`Request as made to ${req.originalUrl}`)
        return next()
    })
    
    server.use('/api', router)
    server.use('/api/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile))
}

module.exports = {
    init: init
}
